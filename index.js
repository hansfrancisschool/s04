/*Demo*/
document.getElementById("btn-1").addEventListener('click', ()=> {alert("Add More!");
});

let paragraph = document.getElementById("paragraph-1");

let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', () => {
	paragraph.innerHTML = "I can even do this!";
});

document.getElementById("btn-3").addEventListener('click', () => {
	paragraph2.innerHTML = "Or this!";
	paragraph2.style.color = "purple";
	paragraph2.style.fontSize= "50px";
});

// console edit
console.log("Hello, my name is Thonie. I purple you");

console.log("thonie"); //log this message on the console

/*Variables*/

/*
In HTML, elements are containers of other elements and text.
In JavaScript, variables are containers of data. A given name is used to describe a piece of data.

Variables also allow us to use or refer to data multiple times
*/

// Num is our variable
// 10 being the value
let num = 10;

console.log(6);
console.log(num);

let name = "Jin";
console.log("V");
console.log(name);

// Creating variables

/*
To create a variable, there are 2 steps to be done:
	-Declaration which allows to create the variable
	-Initialization which allows to add an initial value to a variable

Variables in JS are declared with the use of let or const keyword.
*/

let myVariable;
// Unassigned variables will return undefined (data type)
console.log(myVariable);

myVariable = "New Initialized Value";
console.log(myVariable);

/*
let myVariable2;
myVariable2 = "Initial Value";
//let myVariable2; (Error)
console.log(myVariable2);

Note: You cannot access before initialization of a variable.

Can you use to refer to variable, that has not been declared or Created? No.

Undefined vs Not defined
Undefined - variable has been declared but no initial value
Not defined - variable you are trying to access does NOT exist.
*/


let myVariable3 = "Another sample";
console.log(myVariable3);

/*Let vs const
Let - we can create variables that can be declared or changed
Const - unchangable
*/

let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);

// re-assign

bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

const pi = 3.1416;
console.log(pi);

const mvp = "Michael Jordan";
console.log(mvp);

let num_sum = 5000;
let numSum = 6000;

console.log(num_sum);
console.log(numSum);

// Declaring multiple vars

let brand = "Toyota", model = "Vios", type = "Sedan";
console.log(brand);
console.log(model);
console.log(type);

console.log(brand, model, type);

/*
Data Types
	In most prog languages, data is differentiated by their types
*/

let country = "Philippines";
let province = 'Metro Manila';

console.log(country);
console.log(province);

let divide = "----------------";
console.log(divide);

// Mini Activity
let firstName = "Hans Francis";
let lastName = "Lugo";

console.log(firstName, lastName);

let fullName = firstName + "" + lastName;
console.log(fullName);

let word1 = "is";
let word2 = "student";
let word3 = "of";
let word4 = "University";
let word5 = "De La Salle";
let word6 = "a";
let word7 = "Dasmariñas";
let space = " ";

// Mini Activity B

let sentence = fullName + space + word1 + space + word6 + space + word2 + space + word3 + space + word5 + space + word4 + space + word7;

console.log(sentence);

// Data type integer
let numString = '5';
let numString2 = '6';
let num1 = 5;
let num2 = 6;
console.log(numString + numString2);
console.log(num1 + num2);

let num3 = 5.5;
let num4 = .5;

console.log(num1 + num3);
console.log(num3 + num4);

console.log(numString + num1);
console.log(num3 + numString2);

console.log(parseInt(numString) + num1);

